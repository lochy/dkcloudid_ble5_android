# DKCloudID_BLE_Android

#### 介绍
深圳市德科物联技术有限公司的蓝牙身份证阅读器Demo, 支持DK309S、DK310等设备。更多产品信息请访问[德科官网](http://www.derkiot.com/)。

### 如何集成到项目中
 **Step 1. Add the JitPack repository to your build file**
 
打开根build.gradle文件，将maven { url 'https://jitpack.io' }添加到repositories的末尾

```
allprojects {
    repositories {
    ...
    maven { url 'https://jitpack.io' }
    }
}
```
 **Step 2. 添加 implementation 'com.gitee.lochy:dkcloudid-ble-android-sdk:v2.0.3' 到dependency** 

```

dependencies {
        implementation 'com.gitee.lochy:dkcloudid-ble-android-sdk:v2.0.3'
}
```

 **Step 3. 在AndroidManifest.xml中添加网络权限、蓝牙搜索权限、蓝牙权限** 
 
 ```

    <uses-permission android:name="android.permission.BLUETOOTH"
        android:maxSdkVersion="30" />
    <uses-permission android:name="android.permission.BLUETOOTH_ADMIN"
        android:maxSdkVersion="30" />
    <uses-permission android:name="android.permission.BLUETOOTH_SCAN" />
    <uses-permission android:name="android.permission.BLUETOOTH_ADVERTISE" />
    <uses-permission android:name="android.permission.BLUETOOTH_CONNECT" />

    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
    <uses-permission android:name="android.permission.FINE_LOCATION" />

    <uses-permission android:name="android.permission.WRITE_CONTACTS"/>
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
    <uses-permission android:name="android.permission.INTERNET" />
```
 
 
 **Step 4. 添加蓝牙搜索权限并初始化蓝牙搜索、设备** 

```
    // 动态申请权限
    private void initPermission(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
            // Android 版本大于等于 Android12 时
            // 只包括蓝牙这部分的权限，其余的需要什么权限自己添加
            mPermissionList.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            mPermissionList.add(Manifest.permission.ACCESS_FINE_LOCATION);
            mPermissionList.add(Manifest.permission.BLUETOOTH_SCAN);
            mPermissionList.add(Manifest.permission.BLUETOOTH_ADVERTISE);
            mPermissionList.add(Manifest.permission.BLUETOOTH_CONNECT);
        } else {
            // Android 版本小于 Android12 及以下版本
            mPermissionList.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            mPermissionList.add(Manifest.permission.ACCESS_FINE_LOCATION);
            mPermissionList.add(Manifest.permission.BLUETOOTH_SCAN);
            mPermissionList.add(Manifest.permission.BLUETOOTH_ADVERTISE);
            mPermissionList.add(Manifest.permission.BLUETOOTH_CONNECT);
        }

        ActivityCompat.requestPermissions(this, mPermissionList.toArray(new String[0]),1001);
    }
	
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION:
                if ( (grantResults != null) && (grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED) ) {  //申请权限成功
                    // TODO request success
                    //ble_nfc服务初始化
                    Intent gattServiceIntent = new Intent(this, BleNfcDeviceService.class);
                    bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
                }
                break;
        }
    }
```

 **Step 5. 添加蓝牙搜索回调** 
 
```

    //Scanner 回调
    private ScannerCallback scannerCallback = new ScannerCallback() {
        @Override
        public void onReceiveScanDevice(BluetoothDevice device, int rssi, byte[] scanRecord) {
            super.onReceiveScanDevice(device, rssi, scanRecord);

            //搜索蓝牙设备并记录信号强度最强的设备
            if ( (scanRecord != null) && (StringTool.byteHexToSting(scanRecord).contains("017f5450")) ) {  //从广播数据中过滤掉其它蓝牙设备
                //已经连接了设备，退出
                if ( bleNfcDevice.isConnection() == BleManager.STATE_CONNECTED ) {
                    return;
                }

				//停止搜索蓝牙设备
                mScanner.stopScan();
				
				//开始连接
                bleNfcDevice.requestConnectBleDevice(device.getAddress());
            }
        }
```

 **Step 6. 添加读卡回调和读卡代码** 

```

    //设备操作类回调
    private DeviceManagerCallback deviceManagerCallback = new DeviceManagerCallback() {
		@Override
        public void onReceiveConnectBtDevice(boolean blnIsConnectSuc) {
            super.onReceiveConnectBtDevice(blnIsConnectSuc);
            if (blnIsConnectSuc) {
                System.out.println("Activity设备连接成功");

                //连接上后延时500ms后再开始发指令
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(500L);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        //设备配置和设备信息读取
                        try {
                            boolean isSuc;

                            /*设置蓝牙5传输*/
                            isSuc = bleNfcDevice.setBle5Mode();
                            if (isSuc) {
                                System.out.println("设置蓝牙5传输成功！");
                            }
                            else {
                                System.out.println("设置蓝牙5传输失败！");
                            }

                            //开始自动寻卡
                            startAutoSearchCard;
                        } catch (DeviceNoResponseException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        }

        @Override
        public void onReceiveDisConnectDevice(boolean blnIsDisConnectDevice) {
            super.onReceiveDisConnectDevice(blnIsDisConnectDevice);
            System.out.println("Activity设备断开链接");
        }
	
        @Override
        //寻到卡片回调
        public void onReceiveRfnSearchCard(boolean blnIsSus, int cardType, byte[] bytCardSn, byte[] bytCarATS) {
            super.onReceiveRfnSearchCard(blnIsSus, cardType, bytCardSn, bytCarATS);
            if (!blnIsSus || cardType == UsbNfcDevice.CARD_TYPE_NO_DEFINE) {
                return;
            }

            System.out.println("Activity接收到激活卡片回调：UID->" + StringTool.byteHexToSting(bytCardSn) + " ATS->" + StringTool.byteHexToSting(bytCarATS));

            final int cardTypeTemp = cardType;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    boolean isReadWriteCardSuc;
                    try {
                        if (bleNfcDevice.isAutoSearchCard()) {
                            //如果是自动寻卡的，寻到卡后，先关闭自动寻卡
                            bleNfcDevice.stoptAutoSearchCard();
                            isReadWriteCardSuc = readWriteCardDemo(cardTypeTemp);
							
                            //读卡结束，重新打开自动寻卡
                            startAutoSearchCard();
                        }
                        else {
                            isReadWriteCardSuc = readWriteCardDemo(cardTypeTemp);

                            //如果不是自动寻卡，读卡结束,关闭天线
                            bleNfcDevice.closeRf();
                        }

                        //打开蜂鸣器提示读卡完成
                        if (isReadWriteCardSuc) {
                            bleNfcDevice.openBeep(50, 50, 3);  //读写卡成功快响3声
                        }
                        else {
                            bleNfcDevice.openBeep(100, 100, 2); //读写卡失败慢响2声
                        }
                    } catch (DeviceNoResponseException e) {
                        e.printStackTrace();
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    };
    
    //读写卡Demo
    private synchronized boolean readWriteCardDemo(int cardType) {
        if (cardType == DeviceManager.CARD_TYPE_ISO4443_B) {  //寻到 B cpu卡、身份证
            final Iso14443bCard iso14443bCard = (Iso14443bCard) bleNfcDevice.getCard();
            if (iso14443bCard != null) {
                SamVIdCard samVIdCard = new SamVIdCard(bleNfcDevice);
                idCard = new IDCard(samVIdCard);

                int cnt = 0;
                do {
                    try {
                        /**
                         * 获取身份证数据
                         * 注意：此方法为同步阻塞方式，需要一定时间才能返回身份证数据，期间身份证不能离开读卡器！
                         */
                        IDCardData idCardData = idCard.getIDCardData();

                        /**
                         * 显示身份证数据
                         */
                        showIDCardData(idCardData);
                        
                        //返回读取成功
                        return true;
                    } catch (DKCloudIDException e) {   //服务器返回异常，重复5次解析
                        e.printStackTrace();
                    }
                    catch (CardNoResponseException e) {    //卡片读取异常，直接退出，需要重新读卡
                        e.printStackTrace();
                        return false;
                    }
                }while ( cnt++ < 5 );  //如果服务器返回异常则重复读5次直到成功
            }
        }
        
        return false;
    }
```
