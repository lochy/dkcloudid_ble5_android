package com.huang.lochy;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.dk.bleNfc.BleManager.BleManager;
import com.dk.bleNfc.BleManager.Scanner;
import com.dk.bleNfc.BleManager.ScannerCallback;
import com.dk.bleNfc.BleNfcDeviceService;
import com.dk.bleNfc.DeviceManager.BleNfcDevice;
import com.dk.bleNfc.DeviceManager.ComByteManager;
import com.dk.bleNfc.DeviceManager.DeviceManager;
import com.dk.bleNfc.DeviceManager.DeviceManagerCallback;
import com.dk.bleNfc.DeviceManager.HwSwVesion;
import com.dk.bleNfc.Exception.CardNoResponseException;
import com.dk.bleNfc.Exception.DKCloudIDException;
import com.dk.bleNfc.Exception.DeviceNoResponseException;
import com.dk.bleNfc.Tool.MyTTS;
import com.dk.bleNfc.Tool.StringTool;
import com.dk.bleNfc.card.CpuCard;
import com.dk.bleNfc.card.FeliCa;
import com.dk.bleNfc.card.Iso14443bCard;
import com.dk.bleNfc.card.Iso15693Card;
import com.dk.bleNfc.card.Mifare;
import com.dk.bleNfc.card.Ntag21x;
import com.dk.bleNfc.card.SamVIdCard;
import com.dkcloudid.DKCloudID;
import com.dkcloudid.IDCard;
import com.dkcloudid.IDCardData;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.crypto.NoSuchPaddingException;

import static com.dk.bleNfc.DeviceManager.BleNfcDevice.DK_LED_STATUS_RED;
import static com.dk.bleNfc.DeviceManager.BleNfcDevice.DK_TTS_SPEED_4;

public class MainActivity extends Activity {
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    BleNfcDeviceService mBleNfcDeviceService;
    private BleNfcDevice bleNfcDevice;
    private Scanner mScanner;
    private Button searchButton = null;
    private EditText msgText = null;
    private EditText atEditText = null;
    private Button atSendButton = null;
    private ProgressDialog readWriteDialog = null;

    private MyTTS myTTS;
    static long time_start = 0;
    static long time_end = 0;
    IDCard idCard = null;

    private static volatile StringBuffer msgBuffer;
    private BluetoothDevice mNearestBle = null;
    private int lastRssi = -100;

    private List<String> mPermissionList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        msgBuffer = new StringBuffer();

        searchButton = (Button) findViewById(R.id.searchButton);
        Button sendButton = (Button) findViewById(R.id.sendButton);
        Button changeBleNameButton = (Button) findViewById(R.id.changeBleNameButton);
        msgText = (EditText) findViewById(R.id.msgText);
        Button clearButton = (Button) findViewById(R.id.clearButton);

        Button openBeepButton = (Button) findViewById(R.id.openBeepButton);
        Button closeBeepButton = (Button) findViewById(R.id.closeBeepButton);
        Button openAntiLostButton = (Button) findViewById(R.id.openAntiLostButton);
        Button closeAntiLostButton = (Button) findViewById(R.id.closeAntiLostButton);
        Button openAutoSearchCard = (Button) findViewById(R.id.openAutoSearchCard);
        Button closeAutoSearchCard = (Button) findViewById(R.id.closeAutoSearchCard);

//        msgText.setKeyListener(null);
//        msgText.setTextIsSelectable(true);
        myTTS = new MyTTS(this);

        readWriteDialog = new ProgressDialog(MainActivity.this);
        readWriteDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        // 设置ProgressDialog 标题
        readWriteDialog.setTitle("请稍等");
        // 设置ProgressDialog 提示信息
        readWriteDialog.setMessage("正在读写数据……");

        clearButton.setOnClickListener(new claerButtonListener());
        searchButton.setOnClickListener(new StartSearchButtonListener());
        sendButton.setOnClickListener(new SendButtonListener());
        changeBleNameButton.setOnClickListener(new changeBleNameButtonListener());
        openBeepButton.setOnClickListener(new OpenBeepButtonListener());
        closeBeepButton.setOnClickListener(new closeBeepButtonListener());
        openAntiLostButton.setOnClickListener(new OpenAntiLostButtonListener());
        closeAntiLostButton.setOnClickListener(new CloseAntiLostButtonListener());
        openAutoSearchCard.setOnClickListener(new OpenAutoSearchCardButtonListener());
        closeAutoSearchCard.setOnClickListener(new CloseAutoSearchCardButtonListener());

        if (!gpsIsOPen(MainActivity.this)) {
            System.out.println("log:" + "GPS未打开！");

            final AlertDialog.Builder normalDialog =
                    new AlertDialog.Builder(MainActivity.this);
            normalDialog.setTitle("请打开GPS");
            normalDialog.setMessage("搜索蓝牙需要打开GPS");
            normalDialog.setPositiveButton("确定",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //...To-do
                            // 转到手机设置界面，用户设置GPS
                            Intent intent = new Intent(
                                    Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivityForResult(intent, 0); // 设置完成后返回到原来的界面
                        }
                    });
            normalDialog.setNegativeButton("关闭",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //...To-do
                        }
                    });
            // 显示
            normalDialog.show();
        }

        initPermission();

        //ble_nfc服务初始化
        Intent gattServiceIntent = new Intent(this, BleNfcDeviceService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        msgText.setText("BLE_NFC Demo v2.2.0 20180409");
    }

    // 动态申请权限
    private void initPermission(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
            // Android 版本大于等于 Android12 时
            // 只包括蓝牙这部分的权限，其余的需要什么权限自己添加
            mPermissionList.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            mPermissionList.add(Manifest.permission.ACCESS_FINE_LOCATION);
            mPermissionList.add(Manifest.permission.BLUETOOTH_SCAN);
            mPermissionList.add(Manifest.permission.BLUETOOTH_ADVERTISE);
            mPermissionList.add(Manifest.permission.BLUETOOTH_CONNECT);
        } else {
            // Android 版本小于 Android12 及以下版本
            mPermissionList.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            mPermissionList.add(Manifest.permission.ACCESS_FINE_LOCATION);
            mPermissionList.add(Manifest.permission.BLUETOOTH_SCAN);
            mPermissionList.add(Manifest.permission.BLUETOOTH_ADVERTISE);
            mPermissionList.add(Manifest.permission.BLUETOOTH_CONNECT);
        }

        ActivityCompat.requestPermissions(this, mPermissionList.toArray(new String[0]),1001);
    }

    /**
     * 判断GPS是否开启，GPS或者AGPS开启一个就认为是开启的
     * @param context
     * @return true 表示开启
     */
    public static final boolean gpsIsOPen(final Context context) {
        LocationManager locationManager
                = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        // 通过GPS卫星定位，定位级别可以精确到街（通过24颗卫星定位，在室外和空旷的地方定位准确、速度快）
        boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // 通过WLAN或移动网络(3G/2G)确定的位置（也称作AGPS，辅助GPS定位。主要用于在室内或遮盖物（建筑群或茂密的深林等）密集的地方定位）
        boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (gps || network) {
            return true;
        }

        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION:
                if ( (grantResults != null) && (grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED) ) {  //申请权限成功
                    // TODO request success
                    //ble_nfc服务初始化
                    Intent gattServiceIntent = new Intent(this, BleNfcDeviceService.class);
                    bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
                }
                break;
        }
    }

    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            BleNfcDeviceService mBleNfcDeviceService = ((BleNfcDeviceService.LocalBinder) service).getService();
            bleNfcDevice = mBleNfcDeviceService.bleNfcDevice;
            mScanner = mBleNfcDeviceService.scanner;
            mBleNfcDeviceService.setDeviceManagerCallback(deviceManagerCallback);
            mBleNfcDeviceService.setScannerCallback(scannerCallback);

            //开始搜索设备
            mScanner.startScan();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBleNfcDeviceService = null;
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        if (mBleNfcDeviceService != null) {
            mBleNfcDeviceService.setScannerCallback(scannerCallback);
            mBleNfcDeviceService.setDeviceManagerCallback(deviceManagerCallback);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (readWriteDialog != null) {
            readWriteDialog.dismiss();
        }

        unbindService(mServiceConnection);

        //关闭服务器连接
        DKCloudID.Close();
    }

    //Scanner 回调
    private ScannerCallback scannerCallback = new ScannerCallback() {
        @SuppressLint("MissingPermission")
        @Override
        public void onReceiveScanDevice(BluetoothDevice device, int rssi, byte[] scanRecord) {
            super.onReceiveScanDevice(device, rssi, scanRecord);
            System.out.println("Activity搜到设备：" + device.getName()
                    + " 信号强度：" + rssi
                    + " scanRecord：" + StringTool.byteHexToSting(scanRecord));

            msgBuffer.append("搜到设备：").append(device.getName()).append(" 信号强度：").append(rssi).append("\r\n");
            handler.sendEmptyMessage(0);

            //搜索蓝牙设备并记录信号强度最强的设备
            if ( ( (scanRecord != null) && (StringTool.byteHexToSting(scanRecord).contains("017f5450"))) || ( (device.getName() != null) && (device.getName().contains("HZ-1501"))) ) {  //从广播数据中过滤掉其它蓝牙设备
                //信号强度过滤，调整这个数值可以控制连接设备的范围
                if (rssi < -55) {
                    return;
                }

                //已经连接了设备，退出
                if ( bleNfcDevice.isConnection() == BleManager.STATE_CONNECTED ) {
                    return;
                }

                mNearestBle = device;
                lastRssi = rssi;

                mScanner.stopScan();
                bleNfcDevice.requestConnectBleDevice(device.getAddress());
            }
        }

        @Override
        public void onScanDeviceStopped() {
            super.onScanDeviceStopped();
        }
    };

    //设备操作类回调
    private DeviceManagerCallback deviceManagerCallback = new DeviceManagerCallback() {
        @Override
        public void onReceiveConnectBtDevice(boolean blnIsConnectSuc) {
            super.onReceiveConnectBtDevice(blnIsConnectSuc);
            if (blnIsConnectSuc) {
                System.out.println("Activity设备连接成功");
                msgBuffer.delete(0, msgBuffer.length());
                msgBuffer.append("设备连接成功!\r\n");
                if (mNearestBle != null) {
                    msgBuffer.append("设备名称：").append(bleNfcDevice.getDeviceName()).append("\r\n");
                }
                msgBuffer.append("信号强度：").append(lastRssi).append("dB\r\n");
                msgBuffer.append("SDK版本：" + BleNfcDevice.SDK_VERSIONS + "\r\n");
                handler.sendEmptyMessage(0);

                //连接上后延时500ms后再开始发指令
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(500L);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        //设备配置和设备信息读取
                        try {
                            boolean isSuc;

                            /*设置蓝牙5传输*/
                            isSuc = bleNfcDevice.setBle5Mode();
                            if (isSuc) {
                                msgBuffer.append("设置蓝牙5传输成功！").append("\r\n");
                            }
                            else {
                                msgBuffer.append("设置蓝牙5传输失败！").append("\r\n");
                            }
                            handler.sendEmptyMessage(0);

                            try {
                                /*获取设备版本号*/
                                HwSwVesion hwSwVesion = bleNfcDevice.getHwSwVesion();
                                msgBuffer.append("硬件版本:").append(String.format("%02x", hwSwVesion.hw_vesion)).append("\r\n")
                                        .append("软件版本: ").append(String.format("%02x", hwSwVesion.sw_vesion)).append("\r\n");
                                handler.sendEmptyMessage(0);
                            }catch (DeviceNoResponseException e) {
                                e.printStackTrace();
                            }

                            /*获取设备电池电压*/
                            double voltage = bleNfcDevice.getDeviceBatteryVoltage();
                            msgBuffer.append("设备电池电压:").append(String.format("%.2f", voltage)).append("\r\n");
                            if (voltage < 3.61) {
                                msgBuffer.append("设备电池电量低，请及时充电！").append("\r\n");
                            } else {
                                msgBuffer.append("设备电池电量充足！").append("\r\n");
                            }
                            handler.sendEmptyMessage(0);

                            /*设置LED状态红色，亮5秒*/
                            isSuc = bleNfcDevice.LEDStatusSet(DK_LED_STATUS_RED, 5000);
                            if (isSuc) {
                                msgBuffer.append("设置蓝牙5传输成功！").append("\r\n");
                            }
                            else {
                                msgBuffer.append("设置蓝牙5传输失败！").append("\r\n");
                            }
                            handler.sendEmptyMessage(0);

                            /*获取设备序列号*/
                            try {
                                msgBuffer.append("\r\n开始获取设备序列号...\r\n");
                                handler.sendEmptyMessage(0);
                                byte[] serialNumberBytes = bleNfcDevice.getSerialNumber();
                                msgBuffer.append("设备序列号为：").append(StringTool.byteHexToSting(serialNumberBytes)).append("\r\n");;
                            }catch (DeviceNoResponseException e) {
                                e.printStackTrace();
                            }

                            /*开启自动寻卡*/
                            msgBuffer.append("\n开启自动寻卡...\r\n");
                            handler.sendEmptyMessage(0);
                            //开始自动寻卡
                            startAutoSearchCard();
                        } catch (DeviceNoResponseException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        }

        @Override
        public void onReceiveDisConnectDevice(boolean blnIsDisConnectDevice) {
            super.onReceiveDisConnectDevice(blnIsDisConnectDevice);
            System.out.println("Activity设备断开链接");
            msgBuffer.delete(0, msgBuffer.length());
            msgBuffer.append("设备断开链接!");
            handler.sendEmptyMessage(0);
        }

        @Override
        public void onReceiveConnectionStatus(boolean blnIsConnection) {
            super.onReceiveConnectionStatus(blnIsConnection);
            System.out.println("Activity设备链接状态回调");
        }

        @Override
        public void onReceiveInitCiphy(boolean blnIsInitSuc) {
            super.onReceiveInitCiphy(blnIsInitSuc);
        }

        @Override
        public void onReceiveDeviceAuth(byte[] authData) {
            super.onReceiveDeviceAuth(authData);
        }

        @Override
        //寻到卡片回调
        public void onReceiveRfnSearchCard(boolean blnIsSus, int cardType, byte[] bytCardSn, byte[] bytCarATS) {
            super.onReceiveRfnSearchCard(blnIsSus, cardType, bytCardSn, bytCarATS);
            if (!blnIsSus || cardType == BleNfcDevice.CARD_TYPE_NO_DEFINE) {
                return;
            }

            System.out.println("Activity接收到激活卡片回调：UID->" + StringTool.byteHexToSting(bytCardSn) + " ATS->" + StringTool.byteHexToSting(bytCarATS));

            final int cardTypeTemp = cardType;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    boolean isReadWriteCardSuc;
                    try {
                        if (bleNfcDevice.isAutoSearchCard()) {
                            //如果是自动寻卡的，寻到卡后，先关闭自动寻卡
                            bleNfcDevice.stoptAutoSearchCard();
                            isReadWriteCardSuc = readWriteCardDemo(cardTypeTemp);

                            //bleNfcDevice.closeRf();
                            //读卡结束，重新打开自动寻卡
                            startAutoSearchCard();
                        }
                        else {
                            isReadWriteCardSuc = readWriteCardDemo(cardTypeTemp);

                            //如果不是自动寻卡，读卡结束,关闭天线
                            //bleNfcDevice.closeRf();
                        }

                        //打开蜂鸣器提示读卡完成
                        if (isReadWriteCardSuc) {
                            bleNfcDevice.openBeep(50, 50, 3);  //读写卡成功快响3声
                        }
                        else {
                            bleNfcDevice.openBeep(100, 100, 2); //读写卡失败慢响2声
                        }
                    } catch (DeviceNoResponseException e) {
                        e.printStackTrace();
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

        @Override
        public void onReceiveRfmSentApduCmd(byte[] bytApduRtnData) {
            super.onReceiveRfmSentApduCmd(bytApduRtnData);

            System.out.println("Activity接收到APDU回调：" + StringTool.byteHexToSting(bytApduRtnData));
        }

        @Override
        public void onReceiveRfmClose(boolean blnIsCloseSuc) {
            super.onReceiveRfmClose(blnIsCloseSuc);
        }

        @Override
        //按键返回回调
        public void onReceiveButtonEnter(byte keyValue) {
            if (keyValue == DeviceManager.BUTTON_VALUE_SHORT_ENTER) { //按键短按
                System.out.println("Activity接收到按键短按回调");
                msgBuffer.append("按键短按\r\n");
                handler.sendEmptyMessage(0);
            }
            else if (keyValue == DeviceManager.BUTTON_VALUE_LONG_ENTER) { //按键长按
                System.out.println("Activity接收到按键长按回调");
                msgBuffer.append("按键长按\r\n");
                handler.sendEmptyMessage(0);
            }
        }

        @Override
        //二维码结果返回
        public void onReceiveQRCode(String qrcode) {
            System.out.println("接收到扫码返回：" + qrcode);
            msgBuffer.append("\r\n接收到扫码返回：\r\n" + qrcode);
            handler.sendEmptyMessage(0);
        }

        @Override
        //测温枪结果返回
        public void onReceiveTempe(byte[] tempe) {
            System.out.println("接收测温枪返回：" + StringTool.byteHexToSting(tempe));
            msgBuffer.append("\r\n接收测温枪返回：" + StringTool.byteHexToSting(tempe));
            handler.sendEmptyMessage(0);
        }
    };

    //搜索按键监听
    private class StartSearchButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if ( (bleNfcDevice.isConnection() == BleManager.STATE_CONNECTED) ) {
                bleNfcDevice.requestDisConnectDevice();
                return;
            }

            mScanner.startScan();
            msgText.setText("正在搜索设备..");
        }
    }

    //读卡按键监听
    private class SendButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if ( (bleNfcDevice.isConnection() != BleManager.STATE_CONNECTED) ) {
                msgText.setText("设备未连接，请先连接设备！");
                return;
            }
            //寻卡一次
            bleNfcDevice.requestRfmSearchCard(ComByteManager.ISO14443_P4, new DeviceManager.onReceiveRfnSearchCardListener() {
                @Override
                public void onReceiveRfnSearchCard(boolean blnIsSus, int cardType, byte[] bytCardSn, byte[] bytCarATS) {
                    //在此进行验证密码、读写操作

                }
            });
        }
    }

    //修改蓝牙名称按键监听
    private class changeBleNameButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if ( (bleNfcDevice.isConnection() != BleManager.STATE_CONNECTED) ) {
                msgText.setText("设备未连接，请先连接设备！");
                return;
            }

            final EditText inputEditText = new EditText(MainActivity.this);
            //inputEditText.setInputType(InputType.TYPE_CLASS_TEXT);
            //inputEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(16)});
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("修改蓝牙名称")
                    .setMessage("请输入新名称")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setView(inputEditText)
                    .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            final String bleName = inputEditText.getText().toString();
                            if (bleName.length() <= 0) {
                                msgBuffer.delete(0, msgBuffer.length());
                                msgBuffer.append("未输入蓝牙名").append("\r\n");
                                handler.sendEmptyMessage(0);
                                return;
                            }
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        //开始验证
                                        if (bleNfcDevice.changeBleName(bleName)) {
                                            msgBuffer.delete(0, msgBuffer.length());
                                            msgBuffer.append("蓝牙名称修改成功！重启设备后生效。").append("\r\n");
                                            handler.sendEmptyMessage(0);
                                        }
                                        else {
                                            msgBuffer.delete(0, msgBuffer.length());
                                            msgBuffer.append("蓝牙名称修改失败！").append("\r\n");
                                            handler.sendEmptyMessage(0);
                                        }
                                    } catch (DeviceNoResponseException e) {
                                        e.printStackTrace();
                                        msgBuffer.delete(0, msgBuffer.length());
                                        msgBuffer.append("蓝牙名称修改失败！").append("\r\n");
                                        handler.sendEmptyMessage(0);
                                    }
                                }
                            }).start();
                        }
                    })
                    .setNegativeButton("取消", null)
                    .show();
        }
    }

    //清空显示按键监听
    private class claerButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            msgBuffer.delete(0, msgBuffer.length());
            handler.sendEmptyMessage(0);
        }
    }

    //打开蜂鸣器监听
    private class OpenBeepButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if ( (bleNfcDevice.isConnection() != BleManager.STATE_CONNECTED) ) {
                msgText.setText("设备未连接，请先连接设备！");
                return;
            }
//            bleNfcDevice.requestOpenBeep(50, 50, 255, new DeviceManager.onReceiveOpenBeepCmdListener() {
//                @Override
//                public void onReceiveOpenBeepCmd(boolean isSuc) {
//                    if (isSuc) {
//                        msgBuffer.delete(0, msgBuffer.length());
//                        msgBuffer.append("打开蜂鸣器成功");
//                        handler.sendEmptyMessage(0);
//                    }
//                }
//            });

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        bleNfcDevice.TTS(msgText.getText().toString(), DK_TTS_SPEED_4);
                    } catch (DeviceNoResponseException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    //关闭蜂鸣器监听
    private class closeBeepButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if ( (bleNfcDevice.isConnection() != BleManager.STATE_CONNECTED) ) {
                msgText.setText("设备未连接，请先连接设备！");
                return;
            }
            bleNfcDevice.requestOpenBeep(50, 50, 0, new DeviceManager.onReceiveOpenBeepCmdListener() {
                @Override
                public void onReceiveOpenBeepCmd(boolean isSuc) {
                    if (isSuc) {
                        msgBuffer.delete(0, msgBuffer.length());
                        msgBuffer.append("关闭蜂鸣器成功");
                        handler.sendEmptyMessage(0);
                    }
                }
            });
        }
    }

    //打开防丢器功能监听
    private class OpenAntiLostButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if ( (bleNfcDevice.isConnection() != BleManager.STATE_CONNECTED) ) {
                msgText.setText("设备未连接，请先连接设备！");
                return;
            }
            bleNfcDevice.requestAntiLostSwitch(true, new DeviceManager.onReceiveAntiLostSwitchListener() {
                @Override
                public void onReceiveAntiLostSwitch(boolean isSuc) {
                    if (isSuc) {
                        msgBuffer.delete(0, msgBuffer.length());
                        msgBuffer.append("打开防丢器功能成功");
                        handler.sendEmptyMessage(0);
                    }
                }
            });
        }
    }

    //关闭防丢器功能监听
    private class CloseAntiLostButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if ( (bleNfcDevice.isConnection() != BleManager.STATE_CONNECTED) ) {
                msgText.setText("设备未连接，请先连接设备！");
                return;
            }
            bleNfcDevice.requestAntiLostSwitch(false, new DeviceManager.onReceiveAntiLostSwitchListener() {
                @Override
                public void onReceiveAntiLostSwitch(boolean isSuc) {
                    if (isSuc) {
                        msgBuffer.delete(0, msgBuffer.length());
                        msgBuffer.append("关闭防丢器功能成功");
                        handler.sendEmptyMessage(0);
                    }
                }
            });
        }
    }

    //打开自动寻卡按键监听
    private class OpenAutoSearchCardButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if ( (bleNfcDevice.isConnection() != BleManager.STATE_CONNECTED) ) {
                msgText.setText("设备未连接，请先连接设备！");
                return;
            }

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        //打开/关闭自动寻卡，200ms间隔，寻M1/UL卡
                        boolean isSuc = bleNfcDevice.startAutoSearchCard((byte) 20, ComByteManager.ISO14443_P4);
                        if (isSuc) {
                            msgBuffer.delete(0, msgBuffer.length());
                            msgBuffer.append("自动寻卡已打开！\r\n");
                            handler.sendEmptyMessage(0);
                        }
                        else {
                            msgBuffer.delete(0, msgBuffer.length());
                            msgBuffer.append("自动寻卡已关闭！\r\n");
                            handler.sendEmptyMessage(0);
                        }
                    } catch (DeviceNoResponseException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    //关闭自动寻卡按键监听
    private class CloseAutoSearchCardButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if ( (bleNfcDevice.isConnection() != BleManager.STATE_CONNECTED) ) {
                msgText.setText("设备未连接，请先连接设备！");
                return;
            }
            //打开/关闭自动寻卡，100ms间隔，寻M1/UL卡、CPU卡
            bleNfcDevice.requestRfmAutoSearchCard(false, (byte) 20, ComByteManager.ISO14443_P4, new DeviceManager.onReceiveAutoSearchCardListener() {
                @Override
                public void onReceiveAutoSearchCard(boolean isSuc) {
                    if (isSuc) {
                        msgBuffer.delete(0, msgBuffer.length());
                        msgBuffer.append("自动寻卡已打开！\r\n");
                        handler.sendEmptyMessage(0);
                    }
                    else {
                        msgBuffer.delete(0, msgBuffer.length());
                        msgBuffer.append("自动寻卡已关闭！\r\n");
                        handler.sendEmptyMessage(0);
                    }
                }
            });
        }
    }

    //开始自动寻卡
    private boolean startAutoSearchCard() throws DeviceNoResponseException {
        //打开自动寻卡，200ms间隔，寻M1/UL卡
        boolean isSuc = false;
        int falseCnt = 0;
        do {
            isSuc = bleNfcDevice.startAutoSearchCard((byte) 20, ComByteManager.ISO14443_P4);
        }while (!isSuc && (falseCnt++ < 10));
        if (!isSuc){
            //msgBuffer.delete(0, msgBuffer.length());
            msgBuffer.append("不支持自动寻卡！\r\n");
            handler.sendEmptyMessage(0);
        }

        return isSuc;
    }

    //读写卡Demo
    @SuppressLint("DefaultLocale")
    private boolean readWriteCardDemo(int cardType) throws NoSuchPaddingException, NoSuchAlgorithmException {
        switch (cardType) {
            case DeviceManager.CARD_TYPE_ISO4443_B:  //寻到 B cpu卡
                final Iso14443bCard iso14443bCard = (Iso14443bCard) bleNfcDevice.getCard();
                if (iso14443bCard != null) {
                    msgBuffer.delete(0, msgBuffer.length());
                    msgBuffer.append("寻到身份证，正在解析，请勿移动身份证！").append("\r\n");
                    handler.sendEmptyMessage(0);
                    myTTS.speak("正在读卡，请勿移动身份证");

                    SamVIdCard samVIdCard = new SamVIdCard(bleNfcDevice);
                    idCard = new IDCard(samVIdCard);

                    time_start = System.currentTimeMillis();
                    int cnt = 0;
                    do {
                        try {
                            /**
                             * 获取身份证数据，带进度回调，如果不需要进度回调可以去掉进度回调参数或者传入null
                             * 注意：此方法为同步阻塞方式，需要一定时间才能返回身份证数据，期间身份证不能离开读卡器！
                             */
                            IDCardData idCardData = idCard.getIDCardData(new IDCard.onReceiveScheduleListener() {
                                @Override
                                public void onReceiveSchedule(int rate) {  //读取进度回调
                                    showReadWriteDialog("正在读取身份证信息,请不要移动身份证", rate);
                                    if (rate == 100) {
                                        time_end = System.currentTimeMillis();
                                        /**
                                         * 这里已经完成读卡，可以开身份证了，在此提示用户读取成功或者打开蜂鸣器提示可以拿开身份证了
                                         */

                                        myTTS.speak("读取成功");
                                    }
                                }
                            });

                            /**
                             * 显示身份证数据
                             */
                            showIDCardData(idCardData);
                            //返回读取成功
                            return true;
                        } catch (DKCloudIDException e) {   //服务器返回异常，重复5次解析
                            e.printStackTrace();

                            //显示错误信息
                            msgBuffer.delete(0, msgBuffer.length());
                            msgBuffer.append(e.getMessage()).append("\r\n");
                            handler.sendEmptyMessage(0);
                        }
                        catch (CardNoResponseException e) {    //卡片读取异常，直接退出，需要重新读卡
                            e.printStackTrace();

                            //显示错误信息
                            msgBuffer.delete(0, msgBuffer.length());
                            msgBuffer.append(e.getMessage()).append("\r\n");
                            handler.sendEmptyMessage(0);
                            //返回读取失败
                            myTTS.speak("请不要移动身份证");
                            return false;
                        } finally {
                            //读卡结束关闭进度条显示
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (readWriteDialog.isShowing()) {
                                        readWriteDialog.dismiss();
                                    }
                                    readWriteDialog.setProgress(0);
                                }
                            });
                        }
                    }while ( cnt++ < 5 );  //如果服务器返回异常则重复读5次直到成功

                    myTTS.speak("读取失败，请重新刷卡");
                }
                break;
            case DeviceManager.CARD_TYPE_ISO4443_A:   //寻到A CPU卡
                final CpuCard cpuCard = (CpuCard) bleNfcDevice.getCard();
                if (cpuCard != null) {
                    msgBuffer.delete(0, msgBuffer.length());
                    msgBuffer.append("寻到CPU卡->UID:").append(cpuCard.uidToString()).append("\r\n");
                    handler.sendEmptyMessage(0);
                    try{
                        //选择深圳通主文件
                        byte[] bytApduRtnData = cpuCard.transceive(SZTCard.getSelectMainFileCmdByte());
                        if (bytApduRtnData.length <= 2) {
                            System.out.println("不是深圳通卡，当成银行卡处理！");
                            //选择储蓄卡交易文件
                            String cpuCardType;
                            bytApduRtnData = cpuCard.transceive(FinancialCard.getSelectDepositCardPayFileCmdBytes());
                            if (bytApduRtnData.length <= 2) {
                                System.out.println("不是储蓄卡，当成借记卡处理！");
                                //选择借记卡交易文件
                                bytApduRtnData = cpuCard.transceive(FinancialCard.getSelectDebitCardPayFileCmdBytes());
                                if (bytApduRtnData.length <= 2) {
                                    msgBuffer.append("未知CPU卡！");
                                    handler.sendEmptyMessage(0);
                                    return false;
                                }
                                else {
                                    cpuCardType = "储蓄卡";
                                }
                            }
                            else {
                                cpuCardType = "借记卡";
                            }

                            //读交易记录
                            System.out.println("发送APDU指令-读10条交易记录");
                            for (int i = 1; i <= 10; i++) {
                                bytApduRtnData = cpuCard.transceive(FinancialCard.getTradingRecordCmdBytes((byte) i));
                                msgBuffer.append(FinancialCard.extractTradingRecordFromeRturnBytes(bytApduRtnData));
                                handler.sendEmptyMessage(0);
                            }
                        }
                        else {  //深圳通处理流程
                            bytApduRtnData = cpuCard.transceive(SZTCard.getBalanceCmdByte());
                            if (SZTCard.getBalance(bytApduRtnData) == null) {
                                msgBuffer.append("未知CPU卡！");
                                handler.sendEmptyMessage(0);
                                System.out.println("未知CPU卡！");
                                return false;
                            }
                            else {
                                msgBuffer.append("深圳通余额：").append(SZTCard.getBalance(bytApduRtnData));
                                handler.sendEmptyMessage(0);
                                System.out.println("余额：" + SZTCard.getBalance(bytApduRtnData));
                                //读交易记录
                                System.out.println("发送APDU指令-读10条交易记录");
                                for (int i = 1; i <= 10; i++) {
                                    bytApduRtnData = cpuCard.transceive(SZTCard.getTradeCmdByte((byte) i));
                                    msgBuffer.append("\r\n").append(SZTCard.getTrade(bytApduRtnData));
                                    handler.sendEmptyMessage(0);
                                }
                            }
                        }
                    } catch (CardNoResponseException e) {
                        e.printStackTrace();
                        return false;
                    }
                }
                break;
            case DeviceManager.CARD_TYPE_FELICA:  //寻到FeliCa
                FeliCa feliCa = (FeliCa) bleNfcDevice.getCard();
                if (feliCa != null) {
                    msgBuffer.delete(0, msgBuffer.length());
                    msgBuffer.append("读取服务008b中数据块0000的数据：\r\n");
                    handler.sendEmptyMessage(0);
                    byte[] pServiceList = {(byte) 0x8b, 0x00};
                    byte[] pBlockList = {0x00, 0x00, 0x00};
                    try {
                        byte[] pBlockData = feliCa.read((byte) 1, pServiceList, (byte) 1, pBlockList);
                        msgBuffer.append(StringTool.byteHexToSting(pBlockData)).append("\r\n");
                        handler.sendEmptyMessage(0);
                    } catch (CardNoResponseException e) {
                        e.printStackTrace();
                        return false;
                    }
                }
                break;
            case DeviceManager.CARD_TYPE_ULTRALIGHT: //寻到Ultralight卡
                String writeText = System.currentTimeMillis() + "深圳市德科物联技术有限公司，专业非接触式智能卡读写器方案商！深圳市德科物联技术有限公司，专业非接触式智能卡读写器方案商！";
                if (msgText.getText().toString().length() > 0) {
                    writeText = msgText.getText().toString();
                }

                final Ntag21x ntag21x = (Ntag21x) bleNfcDevice.getCard();
                if (ntag21x != null) {
                    msgBuffer.delete(0, msgBuffer.length());
                    msgBuffer.append("寻到Ultralight卡 ->UID:").append(ntag21x.uidToString()).append("\r\n");
                    handler.sendEmptyMessage(0);
                    try {
                        //读写单个块Demo
                        msgBuffer.append("开始读取块0数据：\r\n");
                        handler.sendEmptyMessage(0);
                        byte[] readTempBytes = ntag21x.read((byte) 0);
                        msgBuffer.append("返回：").append(StringTool.byteHexToSting(readTempBytes)).append("\r\n");
                        handler.sendEmptyMessage(0);

                        msgBuffer.append("开始读100个字节数据").append("\r\n");
                        handler.sendEmptyMessage(0);
                        readTempBytes = ntag21x.longReadWithScheduleCallback((byte) 4, (byte) (100 / 4), new Ntag21x.onReceiveScheduleListener() {
                            @Override
                            public void onReceiveSchedule(int rate) {
                                showReadWriteDialog("正在读取数据", rate);
                            }
                        });
                        msgBuffer.append("读取成功：\r\n").append(StringTool.byteHexToSting(readTempBytes)).append("\r\n");
                        showReadWriteDialog("正在读取数据", 100);

                        //读写文本Demo
                        ntag21x.NdefTextWrite("123");
                        String readText = ntag21x.NdefTextRead();
                        msgBuffer.append(readText + "\r\n");
                        handler.sendEmptyMessage(0);

                    } catch (CardNoResponseException e) {
                        e.printStackTrace();
                        msgBuffer.append(e.getMessage()).append("\r\n");
                        showReadWriteDialog("正在写入数据", 0);
                        return false;
                    }
                }
                break;
            case DeviceManager.CARD_TYPE_MIFARE:   //寻到Mifare卡
                final Mifare mifare = (Mifare) bleNfcDevice.getCard();
                if (mifare != null) {
                    msgBuffer.delete(0, msgBuffer.length());
                    msgBuffer.append("寻到Mifare卡->UID:").append(mifare.uidToString()).append("\r\n");
                    msgBuffer.append("开始验证第3块密码\r\n");
                    handler.sendEmptyMessage(0);
                    byte[] key = {(byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff};
                    try {
                        boolean anth = mifare.authenticate((byte) 0x10, Mifare.MIFARE_KEY_TYPE_B, key);
                        if (anth) {
                            byte[] readDataBytes = mifare.read((byte) 0x10);
                            msgBuffer.append("块16数据:").append(StringTool.byteHexToSting(readDataBytes)).append("\r\n");
                            handler.sendEmptyMessage(0);

                            readDataBytes = mifare.read((byte) 0x11);
                            msgBuffer.append("块17数据:").append(StringTool.byteHexToSting(readDataBytes)).append("\r\n");
                            handler.sendEmptyMessage(0);
                        }
                        else {
                            msgBuffer.append("验证密码失败\r\n");
                            handler.sendEmptyMessage(0);
                            return false;
                        }
                    } catch (CardNoResponseException e) {
                        e.printStackTrace();
                        return false;
                    }
                }
                break;
            case DeviceManager.CARD_TYPE_ISO15693: //寻到15693卡
                final Iso15693Card iso15693Card = (Iso15693Card) bleNfcDevice.getCard();
                if (iso15693Card != null) {
                    msgBuffer.delete(0, msgBuffer.length());
                    msgBuffer.append("寻到15693卡->UID:").append(iso15693Card.uidToString()).append("\r\n");
                    msgBuffer.append("读块0数据\r\n");
                    handler.sendEmptyMessage(0);
                    try {
                        byte[] cmdBytes = new byte[11];
                        cmdBytes[0] = 0x22;
                        cmdBytes[1] = 0x20;
                        System.arraycopy(iso15693Card.uid, 0, cmdBytes, 2, 8);
                        cmdBytes[10] = 0x01;

                        msgBuffer.append("指令透传发送：" + StringTool.byteHexToSting(cmdBytes) + "\r\n");
                        handler.sendEmptyMessage(0);
                        byte[] returnBytes = iso15693Card.transceive(cmdBytes);
                        msgBuffer.append("返回：" + StringTool.byteHexToSting(returnBytes)).append("\r\n");

                        //读写单个块Demo
                        msgBuffer.append("写数据01020304到块4").append("\r\n");
                        handler.sendEmptyMessage(0);
                        boolean isSuc = iso15693Card.write((byte)4, new byte[] {0x01, 0x02, 0x03, 0x04});
                        if (isSuc) {
                            msgBuffer.append("写数据成功！").append("\r\n");
                            handler.sendEmptyMessage(0);
                        }
                        else {
                            msgBuffer.append("写数据失败！").append("\r\n");
                            handler.sendEmptyMessage(0);
                        }
                        msgBuffer.append("读块4数据").append("\r\n");
                        handler.sendEmptyMessage(0);
                        byte[] bytes = iso15693Card.read((byte) 4);
                        msgBuffer.append("块4数据：").append(StringTool.byteHexToSting(bytes)).append("\r\n");
                        handler.sendEmptyMessage(0);

                        //读写多个块Demo
                        msgBuffer.append("写数据0102030405060708到块5、6").append("\r\n");
                        handler.sendEmptyMessage(0);
                        isSuc = iso15693Card.writeMultiple((byte)5, (byte)2, new byte[] {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08});
                        if (isSuc) {
                            msgBuffer.append("写数据成功！").append("\r\n");
                            handler.sendEmptyMessage(0);
                        }
                        else {
                            msgBuffer.append("写数据失败！").append("\r\n");
                            handler.sendEmptyMessage(0);
                        }
                        msgBuffer.append("读块5、6数据").append("\r\n");
                        handler.sendEmptyMessage(0);
                        bytes = iso15693Card.ReadMultiple((byte) 5, (byte)2);
                        msgBuffer.append("块5、6数据：").append(StringTool.byteHexToSting(bytes)).append("\r\n");
                        handler.sendEmptyMessage(0);
                    } catch (CardNoResponseException e) {
                        e.printStackTrace();
                        return false;
                    }
                }
                break;
        }
        return true;
    }

    //发送读写进度条显示Handler
    private void showReadWriteDialog(String msg, int rate) {
        Message message = new Message();
        message.what = 4;
        message.arg1 = rate;
        message.obj = msg;
        handler.sendMessage(message);
    }

    //显示身份证数据
    private void showIDCardData(IDCardData idCardData) {
        final IDCardData theIDCardData = idCardData;

        //显示照片和指纹
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                msgText.setText("解析成功，读卡用时:" + (time_end - time_start) + "ms\r\n" + theIDCardData.toString() + "\r\n");

                //获取指纹数据
                String fingerprintString = "";
                if (theIDCardData.fingerprintBytes != null && theIDCardData.fingerprintBytes.length > 0) {
                    fingerprintString = "\r\n指纹数据：\r\n" + StringTool.byteHexToSting(theIDCardData.fingerprintBytes);
                }

                SpannableString ss = new SpannableString(msgText.getText().toString()+"[smile]");
                //得到要显示图片的资源
                Drawable d = new BitmapDrawable(theIDCardData.PhotoBmp); //Drawable.createFromPath("mnt/sdcard/photo.bmp");
                //设置高度
                d.setBounds(0, 0, d.getIntrinsicWidth() * 10, d.getIntrinsicHeight() * 10);
                //跨度底部应与周围文本的基线对齐
                ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
                //附加图片
                ss.setSpan(span, msgText.getText().length(),msgText.getText().length()+"[smile]".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                msgText.setText(ss);

                //显示指纹数据
                msgText.append(fingerprintString);
            }
        });
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            msgText.setText(/*"解析成功次数：" + suc_cnt + "\r\n解析失败次数：" + err_cnt + */ msgBuffer + "\r\n");

            if ( (bleNfcDevice.isConnection() == BleManager.STATE_CONNECTED) || ((bleNfcDevice.isConnection() == BleManager.STATE_CONNECTING)) ) {
                searchButton.setText("断开连接");
            }
            else {
                searchButton.setText("搜索设备");
            }

            switch (msg.what) {
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;

                case 4:   //读写进度条
                    if ((msg.arg1 == 0) || (msg.arg1 == 100)) {
                        readWriteDialog.dismiss();
                        readWriteDialog.setProgress(0);
                    } else {
                        readWriteDialog.setMessage((String) msg.obj);
                        readWriteDialog.setProgress(msg.arg1);
                        if (!readWriteDialog.isShowing()) {
                            readWriteDialog.show();
                        }
                    }
                    break;
                case 7:  //搜索设备列表
                    break;
            }
        }
    };
}
